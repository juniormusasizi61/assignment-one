#include <stdio.h>
#include <math.h>

float get_discriminant(float x, float y, float z);
float get_first_root(float x, float y, float z, float discriminant);
float get_second_root(float x, float y, float z, float discriminant);


int main(int argc, char const *argv[])
{
	float a, b, c, discriminant, x, y;

	printf("Enter values for coefficients\n");
	scanf("%f %f %f", &a, &b, &c);
	if (a == 0) 
	{
		printf("coefficient a is zero, please check your input\n");
		return 0;
	}

	discriminant = get_discriminant(a, b, c);

	if (discriminant > 0)
	{
		x = get_first_root(a, b, c, discriminant);
		y = get_second_root(a, b, c, discriminant);
		printf("The roots are real and distinct: \n x = %.2f\n y = %.2f\n", x, y);
	} else if (discriminant == 0) {
		float roots;
		roots = get_first_root(a, b, c, discriminant);
		printf("The roots are real and equal: \n x = y = %.2f\n", roots);
	} else {
		x = get_first_root(a, b, c, discriminant);
		y = get_second_root(a, b, c, discriminant);
		printf("The roots are complex and distinct: \n x = %.2f\n y = %.2f\n", x, y);
	}
	
	return 0;
}

float get_discriminant(float x, float y, float z) {
	return (y * y) - (4 * x * z);
}

float get_first_root(float x, float y, float z, float discriminant) {
	return ((-y) + sqrt(discriminant)) / (2 * x * z);
}

float get_second_root(float x, float y, float z, float discriminant) {
	return ((-y) - sqrt(discriminant)) / (2 * x * z);
}
