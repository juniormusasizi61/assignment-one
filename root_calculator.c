#include <stdio.h>
#include <math.h>

//beginning of the main function
int main() {
    float a, b, c, discriminant, x, y, realPart, imaginaryPart;

//prompting the user to enter the coefficients of the equation
    printf("Enter coefficients a, b, and c: \n");
    scanf("%f %f %f", &a, &b, &c);

//the formula for calculating the discriminant is given by (b^2-4*a*c)
    discriminant = (b * b - 4 * a * c);

    //for the coefficient a being zero, our function returns an error message 
    if (a==0){
        printf('coefficient a is zero, check your values');
    }
//for real and distinct roots,
    if (discriminant > 0) {
        x= (-b + sqrt(discriminant)) / (2 * a);
        y = (-b - sqrt(discriminant)) / (2 * a);

        printf("Roots are real and distinct:\n %.2f and %.2f\n", x, y);
    } 
    //for real and equal
    else if (discriminant == 0) {
        x = y = -b / (2 * a);
        printf("Roots are real and equal:\n %.2f and %.2f\n", x, y);
    }
    // fro complex roots,
     else {
        realPart = -b / (2 * a);
        imaginaryPart = sqrt(-discriminant) / (2 * a);
        printf("Roots are complex and different:\n %.2f+%.2fi and %.2f-%.2fi\n",
               realPart, imaginaryPart, realPart, imaginaryPart);
    }

    return 0;
}